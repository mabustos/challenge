package com.journi.challenge.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.journi.challenge.model.entity.Country;
import com.journi.challenge.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.geo.GeoModule;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonModule;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeospatialIndex;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

/*
    Data initializer class that populates MongoDB instance with stored countries metadata.
    Coordinates are parsed to GeoJsonPoint for future improvement in querying distances.
 */
@Component
public class DataInitializer {

    @Autowired
    private CountryService countryService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.registerModule(new GeoJsonModule());
        ClassPathResource res = new ClassPathResource("data/countries_metadata.json");
        File file = null;
        try {
            file = res.getFile();
            List<Country> countries = mapper.readValue(file, new TypeReference<List<Country>>(){});
            countries.stream().forEach( country ->
                    country.setLocation(new GeoJsonPoint(Double.parseDouble(country.getLng()),Double.parseDouble(country.getLat())))
                    );
            countryService.insertListCountries(countries);
        } catch (JsonParseException e) {
            throw new RuntimeException("Error while trying to parse JSON file "+e);
        } catch (JsonMappingException e) {
            throw new RuntimeException("Error while trying to map JSON values "+e);
        } catch (IOException e) {
            throw new RuntimeException("Input JSON data file has not been found "+e);
        }
    }
}
