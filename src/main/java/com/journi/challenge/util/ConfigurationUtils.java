package com.journi.challenge.util;

import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.Default;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "app")
public class ConfigurationUtils {

    private int numberNearestCountries;
    private String ipStackServiceAddress;

    public int getNumberNearestCountries() {
        return numberNearestCountries;
    }

    public void setNumberNearestCountries(int numberNearestCountries) {
        this.numberNearestCountries = numberNearestCountries;
    }

    public String getIpStackServiceAddress() {
        return ipStackServiceAddress;
    }

    public void setIpStackServiceAddress(String ipStackServiceAddress) {
        this.ipStackServiceAddress = ipStackServiceAddress;
    }
}
