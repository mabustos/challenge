package com.journi.challenge.service;

import com.journi.challenge.model.entity.Country;
import com.journi.challenge.model.repository.CountryRepository;
import com.journi.challenge.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepository countryRepository;

    /**
     * Query that obtains matching names countries and sort them by distance against requester's country.
     *
     * @param country Country that will be considered for calculating distances.
     * @param pattern Text that matches the first text of stored countries' names.
     * @param k Max quantity of returned countries.
     * @return k nearest countries sorted by distance from the requester's country that match the pattern text
     */
    @Override
    public List<Country> getKClosestCountries(Country country, String pattern, int k) {
        List<Country> countries = countryRepository.getMatchingCountries(country, pattern);
        countries.stream().forEach( countryElement -> countryElement.setCalculatedDistance(
                MiscUtils.distance(
                        country.getDoubleLocation(),
                        countryElement.getDoubleLocation(), 0,0)
        ));
        Collections.sort(countries, Comparator.comparing(Country::getCalculatedDistance));
        countries = countries.subList(0, k<countries.size()?k:countries.size());
        return countries;
    }

    @Override
    public List<Country> insertListCountries(List<Country> listCountries) {
         return countryRepository.saveAll(listCountries);
    }

    @Override
    public Country getCountry(String name) {
        return countryRepository.findByName(name);
    }
}
