package com.journi.challenge.service;

import com.journi.challenge.model.entity.Country;

import java.util.List;

public interface CountryService {
    List<Country> getKClosestCountries(Country country, String pattern, int k);
    List<Country> insertListCountries(List<Country> listCountries);
    Country getCountry(String name);
}
