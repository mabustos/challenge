package com.journi.challenge.controller;

import com.journi.challenge.model.entity.Country;
import com.journi.challenge.model.entity.CountryDTO;
import com.journi.challenge.model.entity.IPStackResponse;
import com.journi.challenge.service.CountryService;
import com.journi.challenge.util.ConfigurationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

/*
    Country controller handles all search queries for searching countries.
 */
@Controller
@RequestMapping("/api/search")
public class CountryController {
    @Autowired
    private ConfigurationUtils configurationUtils;

    @Autowired
    private RestTemplate template;

    // Added to map from Entity to DTO
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    CountryService countryService;

    /**
     * This method returns the k nearest countries sorted by distance from the requester's country that match the pattern text
     * @param pattern Text of the search query. It is considered the first text of the match.
     * @return JSON response with an array of CountryDTO mapped elements.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{pattern}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<CountryDTO> getClosestCountries( @PathVariable("pattern") String pattern) {
        IPStackResponse ipStackResponse = template.getForObject(configurationUtils.getIpStackServiceAddress(), IPStackResponse.class);
        Country country = countryService.getCountry(ipStackResponse.getContryName());
        List<Country> countries = countryService.getKClosestCountries(country, pattern, configurationUtils.getNumberNearestCountries());
        return countries.stream()
                .map(post -> convertToDto(post))
                .collect(Collectors.toList());
    }


    private CountryDTO convertToDto(Country country) {
        CountryDTO countryDTO = modelMapper.map(country, CountryDTO.class);
        return countryDTO;
    }
}
