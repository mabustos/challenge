package com.journi.challenge.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
    Controller that handles all the errors in requests. For the moment, it redirects to an error page.
 */
@Controller
public class IssueController implements ErrorController {

    @GetMapping("/error")
    public String redirectNonExistentUrlsToHome(HttpServletResponse response) throws IOException {
        return "error.html"; // TODO: improve the response BODY with error feedback.
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}