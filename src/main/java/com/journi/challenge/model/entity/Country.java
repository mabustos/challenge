package com.journi.challenge.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@Document(collection = "locations")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Country {

    @JsonProperty("scalerank")
    private int scaleRank;
    @JsonProperty("labelrank")
    private int labelRank;
    @JsonProperty("featurecla")
    private String featureCla;
    @JsonProperty("soverignt")
    private String soverignt;
    @JsonProperty("sov_a3")
    private String sovA3;
    @JsonProperty("adm0_dif")
    private String adm0Dif;
    @JsonProperty("level")
    private int level;
    @JsonProperty("type")
    private String type;
    @JsonProperty("admin")
    private String admin;
    @JsonProperty("adm0_a3")
    private String adm0A3;
    @JsonProperty("geou_dif")
    private String geouDif;
    @JsonProperty("geounit")
    private String geounit;
    @JsonProperty("gu_a3")
    private String guA3;
    @JsonProperty("su_dif")
    private String suDif;
    @JsonProperty("subunit")
    private String subunit;
    @JsonProperty("suA3")
    private String su_a3;
    @JsonProperty("name")
    private String name;
    @JsonProperty("abbrev")
    private String abbrev;
    @JsonProperty("postal")
    private String postal;
    @JsonProperty("name_forma")
    private String nameForma;
    @JsonProperty("terr_")
    private String terr;
    @JsonProperty("name_sort")
    private String nameSort;
    @JsonProperty("map_color")
    private String mapColor;
    @JsonProperty("pob_est")
    private Long popEst;
    @JsonProperty("gdp_md_est")
    private Long gdpMdEst;
    @JsonProperty("fips_10_")
    private int fips10;
    @JsonProperty("iso_a2")
    private String isoA2;
    @JsonProperty("iso_a3")
    private String isoA3;
    @JsonProperty("iso_n3")
    private String isoN3;
    @JsonProperty("flag_png")
    private String flagPng;
    //@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;

    @JsonIgnore
    private double calculatedDistance;

    @JsonProperty("lat")
    private String lat;

    @JsonProperty("lng")
    private String lng;

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", flagPng='" + flagPng + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }

    public int getScaleRank() {
        return scaleRank;
    }

    public void setScaleRank(int scaleRank) {
        this.scaleRank = scaleRank;
    }

    public int getLabelRank() {
        return labelRank;
    }

    public void setLabelRank(int labelRank) {
        this.labelRank = labelRank;
    }

    public String getFeatureCla() {
        return featureCla;
    }

    public void setFeatureCla(String featureCla) {
        this.featureCla = featureCla;
    }

    public String getSoverignt() {
        return soverignt;
    }

    public void setSoverignt(String soverignt) {
        this.soverignt = soverignt;
    }

    public String getSovA3() {
        return sovA3;
    }

    public void setSovA3(String sovA3) {
        this.sovA3 = sovA3;
    }

    public String getAdm0Dif() {
        return adm0Dif;
    }

    public void setAdm0Dif(String adm0Dif) {
        this.adm0Dif = adm0Dif;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getAdm0A3() {
        return adm0A3;
    }

    public void setAdm0A3(String adm0A3) {
        this.adm0A3 = adm0A3;
    }

    public String getGeouDif() {
        return geouDif;
    }

    public void setGeouDif(String geouDif) {
        this.geouDif = geouDif;
    }

    public String getGeounit() {
        return geounit;
    }

    public void setGeounit(String geounit) {
        this.geounit = geounit;
    }

    public String getGuA3() {
        return guA3;
    }

    public void setGuA3(String guA3) {
        this.guA3 = guA3;
    }

    public String getSuDif() {
        return suDif;
    }

    public void setSuDif(String suDif) {
        this.suDif = suDif;
    }

    public String getSubunit() {
        return subunit;
    }

    public void setSubunit(String subunit) {
        this.subunit = subunit;
    }

    public String getSu_a3() {
        return su_a3;
    }

    public void setSu_a3(String su_a3) {
        this.su_a3 = su_a3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getNameForma() {
        return nameForma;
    }

    public void setNameForma(String nameForma) {
        this.nameForma = nameForma;
    }

    public String getTerr() {
        return terr;
    }

    public void setTerr(String terr) {
        this.terr = terr;
    }

    public String getNameSort() {
        return nameSort;
    }

    public void setNameSort(String nameSort) {
        this.nameSort = nameSort;
    }

    public String getMapColor() {
        return mapColor;
    }

    public void setMapColor(String mapColor) {
        this.mapColor = mapColor;
    }

    public Long getPopEst() {
        return popEst;
    }

    public void setPopEst(Long popEst) {
        this.popEst = popEst;
    }

    public Long getGdpMdEst() {
        return gdpMdEst;
    }

    public void setGdpMdEst(Long gdpMdEst) {
        this.gdpMdEst = gdpMdEst;
    }

    public int getFips10() {
        return fips10;
    }

    public void setFips10(int fips10) {
        this.fips10 = fips10;
    }

    public String getIsoA2() {
        return isoA2;
    }

    public void setIsoA2(String isoA2) {
        this.isoA2 = isoA2;
    }

    public String getIsoA3() {
        return isoA3;
    }

    public void setIsoA3(String isoA3) {
        this.isoA3 = isoA3;
    }

    public String getIsoN3() {
        return isoN3;
    }

    public void setIsoN3(String isoN3) {
        this.isoN3 = isoN3;
    }

    public String getFlagPng() {
        return flagPng;
    }

    public void setFlagPng(String flagPng) {
        this.flagPng = flagPng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public GeoJsonPoint getLocation() {
        return this.location;
    }

    public void setLocation(GeoJsonPoint geoJsonPoint){
        this.location = geoJsonPoint;
    }

    public double getCalculatedDistance() {
        return calculatedDistance;
    }

    public void setCalculatedDistance(double calculatedDistance) {
        this.calculatedDistance = calculatedDistance;
    }

    public double[] getDoubleLocation(){
        double[] tempLocation = new double[2];
        tempLocation[0] = Double.parseDouble(this.lng);
        tempLocation[1] = Double.parseDouble(this.lat);
        return tempLocation;
    }
}
