package com.journi.challenge.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Entity;

/*
Entity that handles the response from the IPStack API. It ignores not present fields.
 */

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class IPStackResponse {

    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("country_name")
    private String contryName;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContryName() {
        return contryName;
    }

    public void setContryName(String contryName) {
        this.contryName = contryName;
    }
}
