package com.journi.challenge.model.repository;

import com.journi.challenge.model.entity.Country;

import java.util.List;

public interface CustomCountryRepository {
    List<Country> getMatchingCountries(Country country, String pattern);
}
