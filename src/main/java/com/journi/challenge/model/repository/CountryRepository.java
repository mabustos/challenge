package com.journi.challenge.model.repository;

import com.journi.challenge.model.entity.Country;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository  extends MongoRepository<Country, Long>, CustomCountryRepository {
    Country findByName(String name);
}
