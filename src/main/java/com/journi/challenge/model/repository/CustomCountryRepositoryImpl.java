package com.journi.challenge.model.repository;

import com.journi.challenge.model.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;



public class CustomCountryRepositoryImpl implements CustomCountryRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    //
    @Override
    public List<Country> getMatchingCountries(Country country, String pattern) {
        Query query = new Query(Criteria.where("name").regex("^"+pattern,"i"));
        return mongoTemplate.find(query, Country.class);
    }

}
