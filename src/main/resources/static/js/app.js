var app = angular.module('journiChallenge', ["ngResource"]);

app.service('searchService', ['$http', function($http) {
    this.getCountries = function getClosestCountries(pattern) {
        return $http({
            method: 'GET',
            url: 'api/search/' + pattern
        });
    }
}]);

app.controller('journiChallengeController', ['$scope', 'searchService',
    function($scope, searchService) {
        $scope.selectedCountry = "";
        $scope.isSubmitPushed = false;
        $scope.errorsDetected = false;
        $scope.fillTextbox = function(country) {
            $scope.pattern = country.name;
            $scope.isSubmitPushed = false;
            $scope.filterCountry = null;
            $scope.selectedCountry = country;
        }
        $scope.setCard = function() {
            $scope.isSubmitPushed = true;
        }
        $scope.isCountryNotSelected = function(){
            if($scope.isSubmitPushed===false){
                return true;
            }
            else{
                return false;
            }
        }
        $scope.getCountries = function() {
            var pattern = $scope.pattern;
            if ($scope.pattern=="") return;
            $scope.filterCountry = null;
            searchService.getCountries($scope.pattern)
                .then(function success(response) {
                        errorsDetected = false;
                        $scope.filterCountry = response.data;
                    },
                    function error(response) {
                        errorsDetected = true;
                        if (response.status === 404) {
                            $scope.errorMessage = 'Page not found!';
                        } else {
                            $scope.errorMessage = "Error getting country!";
                        }
                    });
        };
    }
]);