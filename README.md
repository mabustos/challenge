# Challenge - Country search

Country search engine that bases its results on the distance of the matched countries against the requester's country.

## How to run it?

mvn spring-boot:run

## Notes

- It is provided with an embedded mongodb instance, which will be downloaded the first time it runs.
- Configuration properties can be found in application.properties (Services API url and Queries parameteres).

